from app.resume.model import Resume
from app.dependant.schema import DependantBaseSchema
from sqlalchemy.orm import Session
# from sqlalchemy import or_
from fastapi import Depends, HTTPException, status, APIRouter #
from app.database import get_db

router = APIRouter()


@router.get('/')
def get_dependants(resumeId: str, db: Session = Depends(get_db)):
    db_resume = db.query(Resume).filter(Resume.id == resumeId).first()
    if not db_resume:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"No resume with this id: {id} found")
    dependants = []
    if db_resume.dependants is not None:
        dependants = db_resume.dependants.split(",")
    return {'status': 'success', 'results': len(dependants), 'dependants': dependants}


@router.post('/', status_code=status.HTTP_201_CREATED)
def create_dependant(resumeId: str, payload: DependantBaseSchema, db: Session = Depends(get_db)):
    resume_query = db.query(Resume).filter(Resume.id == resumeId)
    db_resume = resume_query.first()

    if not db_resume:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'No resume with this id: {resumeId} found')
    dependants = []
    if db_resume.dependants is not None:
        dependants = db_resume.dependants.split(",")
    payloadDependant = payload.dict(exclude_unset=True)

    # if input-language is not exists in list-dependants
    if payloadDependant['title'] not in dependants:
        dependants.append(payloadDependant['title'])
    dependants = ','.join(dependants)
    resume_query.update({"dependants": dependants})
    db.commit()
    return {"status": "success", "message": "Dependant added successfully", "dependants": dependants}
    

@router.delete('/')
def delete_dependant(resumeId: str, payload: DependantBaseSchema, db: Session = Depends(get_db)):
    resume_query = db.query(Resume).filter(Resume.id == resumeId)
    db_resume = resume_query.first()

    if not db_resume:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'No resume with this id: {resumeId} found')
    dependants = []
    if db_resume.dependants is not None:
        dependants = db_resume.dependants.split(",")
    payloadDependant = payload.dict(exclude_unset=True)

    # if input-language is exists in list-dependants
    if payloadDependant['title'] in dependants:
        dependants.remove(payloadDependant['title'])
    dependants = ','.join(dependants)
    resume_query.update({"dependants": dependants})
    db.commit()
    return {"status": "success", "message": "Dependant deleted successfully", "status_code":status.HTTP_204_NO_CONTENT, "dependants": dependants}
