import os
from dotenv import load_dotenv
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
import logging

load_dotenv()

app_title = os.getenv("APP_TITLE")
app = FastAPI(title=app_title)

origins = [
    "http://"+os.getenv("APP_HOST")+":"+os.getenv("APP_PORT"),
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

database_url = os.getenv("DATABASE_URL")

# will create db tables
from app.resume import model
from app.database import engine
model.Base.metadata.create_all(bind=engine)


# Create a logger instance
logger = logging.getLogger(__name__)

# Set the logging level
logger.setLevel(logging.DEBUG)

# Create a console handler
console_handler = logging.StreamHandler()
console_handler.setLevel(logging.DEBUG)

# # Create a formatter and set it for the console handler
# formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
# console_handler.setFormatter(formatter)

# Add the console handler to the logger
logger.addHandler(console_handler)