from app.resume.schema import ResumeBaseSchema
from app.resume.model import Resume
from sqlalchemy.orm import Session
from sqlalchemy import or_
from fastapi import Depends, HTTPException, status, APIRouter
from app.database import get_db
from datetime import datetime
import json

router = APIRouter()


@router.get('/')
def get_resumes(db: Session = Depends(get_db), limit: int = 10, page: int = 1, search: str = ''):
    skip = (page - 1) * limit
    search_fields = [
        Resume.title.contains(search),
        Resume.name.contains(search),
        Resume.objective.contains(search),
        Resume.email.contains(search),
        Resume.mobile.contains(search),
        Resume.languages.contains(search),
        Resume.skills.contains(search),
        # Resume.experiences.contains(search),
        # Resume.educations.contains(search),
        # Resume.certifications.contains(search),
        # Resume.hobbies.contains(search),
        # Resume.projects.contains(search)
    ]
    # Combine the search terms using OR operator
    combined_search = or_(*search_fields)

    resumes = db.query(Resume).filter(combined_search).limit(limit).offset(skip).all()
    return {'status': 'success', "message": "All resumes fetched successfully", 'results': len(resumes), 'resumes': resumes}


@router.post('/', status_code=status.HTTP_201_CREATED)
def create_resume(payload: ResumeBaseSchema, db: Session = Depends(get_db)):
    new_resume = Resume(**payload.dict())
    db.add(new_resume)
    db.commit()
    db.refresh(new_resume)
    return {"status": "success", "message": "Resume created successfully", "resume": new_resume}


@router.patch('/{resumeId}')
def update_resume(resumeId: str, payload: ResumeBaseSchema, db: Session = Depends(get_db)):
    resume_query = db.query(Resume).filter(Resume.id == resumeId)
    db_resume = resume_query.first()

    if not db_resume:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'No resume with this id: {resumeId} found')
    update_data = payload.dict(exclude_unset=True)
    resume_query.filter(Resume.id == resumeId).update(update_data,
                                                       synchronize_session=False)
    db.commit()
    db.refresh(db_resume)
    return {"status": "success", "message": "Resume updated successfully", "resume": db_resume}


@router.get('/{resumeId}')
def get_resume(resumeId: str, db: Session = Depends(get_db)):
    resume = db.query(Resume).filter(Resume.id == resumeId).first()
    if not resume:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"No resume with this id: {id} found")
    
    if resume.certifications is not None:
        resume.certifications = json.loads(resume.certifications)
    if resume.educations is not None:
        resume.educations = json.loads(resume.educations)
    if resume.experiences is not None:
        resume.experiences = json.loads(resume.experiences)
    if resume.projects is not None:
        resume.projects = json.loads(resume.projects)
    if resume.skills is not None:
        resume.skills = json.loads(resume.skills)
    if resume.contacts is not None:
        resume.contacts = json.loads(resume.contacts)
    if resume.languages is not None:
        resume.languages = resume.languages.split(",")
    if resume.hobbies is not None:
        resume.hobbies = resume.hobbies.split(",")
    if resume.dependants is not None:
        resume.dependants = resume.dependants.split(",")  

    return {"status": "success", "message": "Resume fetched successfully", "resume": resume}


@router.delete('/{resumeId}')
def delete_resume(resumeId: str, db: Session = Depends(get_db)):
    resume_query = db.query(Resume).filter(Resume.id == resumeId)
    resume = resume_query.first()
    if not resume:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'No resume with this id: {id} found')
    # resume_query.delete(synchronize_session=False)
    # db.commit()
    resume_query.deleted = datetime.utcnow()  # or use `datetime.now()
    db.commit()
    return {"status": "success", "message": "Resume deleted successfully", "status_code":status.HTTP_204_NO_CONTENT}
