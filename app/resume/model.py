from app.database import Base
from sqlalchemy import Column, TIMESTAMP, Date, String, Text, Boolean, BigInteger, Index
from sqlalchemy.dialects.mysql import JSON
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql import func
# from fastapi_utils.guid_type import GUID, GUID_DEFAULT_SQLITE


class Resume(Base):
    __tablename__ = 'resumes'
    id = Column(BigInteger, primary_key=True, autoincrement=True)
    # id = Column(GUID, primary_key=True, default=GUID_DEFAULT_SQLITE)
    
    # Add an index on the id column
    __table_args__ = (Index('idx_jobportal_resumes_id', 'id'),)
    
    title = Column(String(150), nullable=False)
    objective = Column(Text, nullable=True, default=None)
    name = Column(String(150), nullable=False) 
    email = Column(String(255), nullable=True, default=None)
    mobile = Column(String(12), nullable=True, default=None)
    dob = Column(Date, nullable=True, default=None)
    marital_status = Column(Boolean, default=None)
    guardian_name = Column(String(150), nullable=True, default=None)
    contacts = Column(JSON, nullable=True, default=None, comment='JSON')
    dependants = Column(Text, nullable=True, default=None, comment='Comma Seperated')
    languages = Column(Text, nullable=True, default=None, comment='Comma Seperated')
    skills = Column(JSON, nullable=True, default=None, comment='JSON')
    experiences = Column(JSON, nullable=True, default=None, comment='JSON')
    educations = Column(JSON, nullable=True, default=None, comment='JSON')
    certifications = Column(JSON, nullable=True, default=None, comment='JSON')
    hobbies = Column(Text, nullable=True, default=None, comment='Comma Seperated')
    projects = Column(JSON, nullable=True, default=None, comment='JSON')
    created = Column(TIMESTAMP(timezone=True), nullable=False, server_default=func.now())
    updated = Column(TIMESTAMP(timezone=True), default=None, onupdate=func.now())
    deleted = Column(TIMESTAMP(timezone=True), nullable=True, default=None)



# Define the indexes separately
email_index = Index('idx_jobportal_resumes_email', Resume.email)
mobile_index = Index('idx_jobportal_resumes_mobile', Resume.mobile)
title_index = Index('idx_jobportal_resumes_title', Resume.title)
name_index = Index('idx_jobportal_resumes_name', Resume.name)





