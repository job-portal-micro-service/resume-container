from datetime import date # , datetime
from typing import Optional #, Dict, List
from pydantic import BaseModel, EmailStr, validator

class ResumeBaseSchema(BaseModel):
    title: str                                # string: title
    objective: Optional[str] = None           # text : description
    name: str                                 # string: name
    email: Optional[EmailStr] = None          # email
    mobile: Optional[str] = None              # string: mobile
    dob: Optional[date] = None                # date
    marital_status: Optional[bool] = None     # bool: y/n
    guardian_name: Optional[str] = None       # string: name
    # contacts: Optional[Dict] = None         #JSON
    ## dependants: Optional[str] = None        # text: comma seperated
    ## languages: Optional[str] = None         # text: comma seperated
    ## skills: Optional[Dict] = None           #JSON
    ## experiences: Optional[Dict] = None      #JSON
    ## educations: Optional[Dict] = None       #JSON
    ## certifications: Optional[Dict] = None   #JSON
    ## hobbies: Optional[str] = None           # text: comma seperated
    ## projects: Optional[Dict] = None         #JSON
    # created: datetime | None = None
    # updated: datetime | None = None
    # deleted: datetime | None = None

    # mobile-validation 
    @validator('mobile')
    def check_mobile(cls, v):
        if v is not None:
            # Check if the mobile number consists of 10 digits
            if len(v) != 10 or not v.isdigit():
                raise ValueError("Mobile number must be a 10-digit number")
        return v
    

    class Config:
        from_attributes = True
        allow_population_by_name = True
        arbitrary_types_allowed = True

