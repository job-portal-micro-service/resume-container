from datetime import date 
from typing import Optional, List
from pydantic import BaseModel
# from app.skill.schema import SkillBaseSchema as Skill

class ProjectBaseSchema(BaseModel):
    title: str 
    description: str 
    duration_from: Optional[date] = None 
    duration_to: Optional[date] = None 
    skills: Optional[List[str]] = None
    # skills: Optional[List[Skill]] = None
