from app.resume.model import Resume
from app.project.schema import ProjectBaseSchema
from sqlalchemy.orm import Session
from sqlalchemy import or_
from fastapi import Depends, HTTPException, status, APIRouter
from app.database import get_db
import json
from app.helpers import serialize_date
from app.config import logger

router = APIRouter()


@router.get('/')
def get_projects(resumeId: str, db: Session = Depends(get_db)):
    db_resume = db.query(Resume).filter(Resume.id == resumeId).first()
    if not db_resume:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"No resume with this id: {id} found")
    projects = []
    if db_resume.projects is not None:
        projects = json.loads(db_resume.projects)

    return {'status': 'success', 'results': len(projects), 'projects': projects}


@router.post('/', status_code=status.HTTP_201_CREATED)
def create_project(resumeId: str, payload: ProjectBaseSchema, db: Session = Depends(get_db)):
    resume_query = db.query(Resume).filter(Resume.id == resumeId)
    db_resume = resume_query.first()

    if not db_resume:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'No resume with this id: {resumeId} found')
    projects = []
    if db_resume.projects is not None:
        projects = json.loads(db_resume.projects)
    payloadProject = payload.dict(exclude_unset=True)

    logger.debug({"payloadProject" : payloadProject})
    # updates Date format payloadProject
    if payloadProject['duration_from'] is not None:
        payloadProject['duration_from'] = payloadProject['duration_from'].isoformat() 
    if payloadProject['duration_to'] is not None:
        payloadProject['duration_to'] = payloadProject['duration_to'].isoformat() 

    # if exists return
    if payloadProject in projects:
        return { "status": "success", "message": "Payload already exists", "projects": projects, "payload" : payloadProject }
    
    # if input-project is not exists in list-projects
    projects.append(payloadProject)
    projects = json.dumps(projects, default=serialize_date)
    resume_query.update({"projects": projects})
    
    db.commit()
    projects = json.loads(db_resume.projects)
    return {"status": "success", "message": "Project added successfully", "projects": projects}
    

@router.delete('/')
def delete_project(resumeId: str, payload: ProjectBaseSchema, db: Session = Depends(get_db)):
    resume_query = db.query(Resume).filter(Resume.id == resumeId)
    db_resume = resume_query.first()

    if not db_resume:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'No resume with this id: {resumeId} found')
    projects = []
    if db_resume.projects is not None:
        projects = json.loads(db_resume.projects)
    payloadProject = payload.dict(exclude_unset=True)

    # updates Date format payloadProject
    # updates Date format payloadProject
    if payloadProject['duration_from'] is not None:
        payloadProject['duration_from'] = payloadProject['duration_from'].isoformat() 
    if payloadProject['duration_to'] is not None:
        payloadProject['duration_to'] = payloadProject['duration_to'].isoformat() 
    
    # if not exists return
    if payloadProject not in projects:
        return { "status": "success", "message": "Not Found Project", "projects": projects, "payload" : payloadProject }
        
    projects.remove(payloadProject)
    projects = json.dumps(projects, default=serialize_date)
    resume_query.update({"projects": projects})
    
    db.commit()
    projects = json.loads(db_resume.projects)

    return { "status": "success", "message": "Project deleted successfully", "status_code":status.HTTP_204_NO_CONTENT, "projects": projects }
