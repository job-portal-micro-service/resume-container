import uvicorn
from app import config
from app.config import logger
from app.resume import controller as controllerResume
from app.language import controller as controllerLanguage
from app.dependant import controller as controllerDependant
from app.education import controller as controllerEducation
from app.skill import controller as controllerSkill
from app.experience import controller as controllerExperience
from app.certification import controller as controllerCertification
from app.hobby import controller as controllerHobby
from app.project import controller as controllerProject
from app.contact import controller as controllerContact

app = config.app

app.include_router(controllerResume.router, tags=['Resumes'], prefix='/api/resumes')
app.include_router(controllerLanguage.router, tags=['Languages'], prefix='/api/resume/{resumeId}/languages')
app.include_router(controllerDependant.router, tags=['Dependants'], prefix='/api/resume/{resumeId}/dependants')
app.include_router(controllerEducation.router, tags=['Educations'], prefix='/api/resume/{resumeId}/educations')
app.include_router(controllerSkill.router, tags=['Skills'], prefix='/api/resume/{resumeId}/skills')
app.include_router(controllerExperience.router, tags=['Experiences'], prefix='/api/resume/{resumeId}/experiences')
app.include_router(controllerCertification.router, tags=['Certifications'], prefix='/api/resume/{resumeId}/certifications')
app.include_router(controllerHobby.router, tags=['Hobbies'], prefix='/api/resume/{resumeId}/hobbies')
app.include_router(controllerProject.router, tags=['Projects'], prefix='/api/resume/{resumeId}/projects')
app.include_router(controllerContact.router, tags=['Contacts'], prefix='/api/resume/{resumeId}/contacts')


@app.get("/api/")
def root():
    # Log a debug message
    logger.debug("This is a debug message")
    return {"message": "Welcome to FastAPI with SQLAlchemy"}

