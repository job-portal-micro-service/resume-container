from app.resume.model import Resume
from app.language.schema import LanguageBaseSchema
from sqlalchemy.orm import Session
from sqlalchemy import or_
from fastapi import Depends, HTTPException, status, APIRouter
from app.database import get_db

router = APIRouter()


@router.get('/')
def get_languages(resumeId: str, db: Session = Depends(get_db)):
    db_resume = db.query(Resume).filter(Resume.id == resumeId).first()
    if not db_resume:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"No resume with this id: {id} found")
    languages = []
    if db_resume.languages is not None:
        languages = db_resume.languages.split(",")
    return {'status': 'success', 'results': len(languages), 'languages': languages}


@router.post('/', status_code=status.HTTP_201_CREATED)
def create_language(resumeId: str, payload: LanguageBaseSchema, db: Session = Depends(get_db)):
    resume_query = db.query(Resume).filter(Resume.id == resumeId)
    db_resume = resume_query.first()

    if not db_resume:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'No resume with this id: {resumeId} found')
    languages = []
    if db_resume.languages is not None:
        languages = db_resume.languages.split(",")
    payloadLanguage = payload.dict(exclude_unset=True)

    # if input-language is not exists in list-languages
    if payloadLanguage['title'] not in languages:
        languages.append(payloadLanguage['title'])
    languages = ','.join(languages)
    resume_query.update({"languages": languages})
    db.commit()
    return {"status": "success", "message": "Language added successfully", "languages": languages}
    

@router.delete('/')
def delete_language(resumeId: str, payload: LanguageBaseSchema, db: Session = Depends(get_db)):
    resume_query = db.query(Resume).filter(Resume.id == resumeId)
    db_resume = resume_query.first()

    if not db_resume:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'No resume with this id: {resumeId} found')
    languages = []
    if db_resume.languages is not None:
        languages = db_resume.languages.split(",")
    payloadLanguage = payload.dict(exclude_unset=True)

    # if input-language is exists in list-languages
    if payloadLanguage['title'] in languages:
        languages.remove(payloadLanguage['title'])
    languages = ','.join(languages)
    resume_query.update({"languages": languages})
    db.commit()
    return {"status": "success", "message": "Language deleted successfully", "status_code":status.HTTP_204_NO_CONTENT, "languages": languages}
