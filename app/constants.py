
EducationLabel = (
    'pre-school',
    'intermediate-school',
    'high-school',
    'diploma',
    'degree',
    'masters',
)

EducationType = (
    'vocational',
    'literature',
    'research',
    'management',
)

ProficiencyLabel = (
    'Fundamental',
    'Novice',
    'Intermediate',
    'Advanced',
    'Expert',
)