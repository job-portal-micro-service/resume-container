from datetime import date #, datetime 
from typing import Optional, Literal #, List
from pydantic import BaseModel #, validator, EmailStr

class CertificationBaseSchema(BaseModel):
    title: str 
    organization: str 
    valid_from: Optional[date] = None 
    valid_to: Optional[date] = None 
    