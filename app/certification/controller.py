from app.resume.model import Resume
from app.certification.schema import CertificationBaseSchema
from sqlalchemy.orm import Session
from sqlalchemy import or_
from fastapi import Depends, HTTPException, status, APIRouter
from app.database import get_db
import json
from app.helpers import serialize_date

router = APIRouter()


@router.get('/')
def get_certifications(resumeId: str, db: Session = Depends(get_db)):
    db_resume = db.query(Resume).filter(Resume.id == resumeId).first()
    if not db_resume:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"No resume with this id: {id} found")
    certifications = []
    if db_resume.certifications is not None:
        certifications = json.loads(db_resume.certifications)

    return {'status': 'success', 'results': len(certifications), 'certifications': certifications}


@router.post('/', status_code=status.HTTP_201_CREATED)
def create_certification(resumeId: str, payload: CertificationBaseSchema, db: Session = Depends(get_db)):
    resume_query = db.query(Resume).filter(Resume.id == resumeId)
    db_resume = resume_query.first()

    if not db_resume:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'No resume with this id: {resumeId} found')
    certifications = []
    if db_resume.certifications is not None:
        certifications = json.loads(db_resume.certifications)
    payloadCertification = payload.dict(exclude_unset=True)

    # updates Date format payloadCertification
    if payloadCertification['valid_from'] is not None:
        payloadCertification['valid_from'] = payloadCertification['valid_from'].isoformat() 
    if payloadCertification['valid_to'] is not None:
        payloadCertification['valid_to'] = payloadCertification['valid_to'].isoformat() 

    # if exists return
    if payloadCertification in certifications:
        return { "status": "success", "message": "Payload already exists", "certifications": certifications, "payload" : payloadCertification }
    
    # if input-certification is not exists in list-certifications
    certifications.append(payloadCertification)
    certifications = json.dumps(certifications, default=serialize_date)
    resume_query.update({"certifications": certifications})
    
    db.commit()
    certifications = json.loads(db_resume.certifications)
    return {"status": "success", "message": "Certification added successfully", "certifications": certifications}
    

@router.delete('/')
def delete_certification(resumeId: str, payload: CertificationBaseSchema, db: Session = Depends(get_db)):
    resume_query = db.query(Resume).filter(Resume.id == resumeId)
    db_resume = resume_query.first()

    if not db_resume:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'No resume with this id: {resumeId} found')
    certifications = []
    if db_resume.certifications is not None:
        certifications = json.loads(db_resume.certifications)
    payloadCertification = payload.dict(exclude_unset=True)

    # updates Date format payloadCertification
    # updates Date format payloadCertification
    if payloadCertification['valid_from'] is not None:
        payloadCertification['valid_from'] = payloadCertification['valid_from'].isoformat() 
    if payloadCertification['valid_to'] is not None:
        payloadCertification['valid_to'] = payloadCertification['valid_to'].isoformat() 
    
    # if not exists return
    if payloadCertification not in certifications:
        return { "status": "success", "message": "Not Found Certification", "certifications": certifications, "payload" : payloadCertification }
        
    certifications.remove(payloadCertification)
    certifications = json.dumps(certifications, default=serialize_date)
    resume_query.update({"certifications": certifications})
    
    db.commit()
    certifications = json.loads(db_resume.certifications)

    return { "status": "success", "message": "Certification deleted successfully", "status_code":status.HTTP_204_NO_CONTENT, "certifications": certifications }
