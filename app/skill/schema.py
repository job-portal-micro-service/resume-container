from datetime import date #, datetime
from typing import Optional, Literal #, List, Dict
from pydantic import BaseModel #, EmailStr, validator
from app.constants import ProficiencyLabel

class SkillBaseSchema(BaseModel):
    heading: Optional[str] = None
    title: str 
    proficiency: Optional[Literal[ProficiencyLabel]] = None
    last_used: Optional[date] = None
    experience_in_year: Optional[int] = None
    experience_in_month: Optional[int] = None
    
