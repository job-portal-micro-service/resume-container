from app.resume.model import Resume
from app.skill import schema
from sqlalchemy.orm import Session
from sqlalchemy import or_
from fastapi import Depends, HTTPException, status, APIRouter
from app.database import get_db
import json
from app.helpers import serialize_date

router = APIRouter()


@router.get('/')
def get_skills(resumeId: str, db: Session = Depends(get_db)):
    db_resume = db.query(Resume).filter(Resume.id == resumeId).first()
    if not db_resume:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"No resume with this id: {id} found")
    skills = []
    if db_resume.skills is not None:
        skills = json.loads(db_resume.skills)

    return {'status': 'success', 'results': len(skills), 'skills': skills}


@router.post('/', status_code=status.HTTP_201_CREATED)
def create_skill(resumeId: str, payload: schema.SkillBaseSchema, db: Session = Depends(get_db)):
    resume_query = db.query(Resume).filter(Resume.id == resumeId)
    db_resume = resume_query.first()

    if not db_resume:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'No resume with this id: {resumeId} found')
    skills = []
    if db_resume.skills is not None:
        skills = json.loads(db_resume.skills)
    payloadSkill = payload.dict(exclude_unset=True)

    # updates Date format payloadSkill
    if payloadSkill['last_used'] is not None:
        payloadSkill['last_used'] = payloadSkill['last_used'].isoformat()  

    # if exists return
    if payloadSkill in skills:
        return { "status": "success", "message": "Payload already exists", "skills": skills, "payload" : payloadSkill }
    
    # if input-skill is not exists in list-skills
    skills.append(payloadSkill)
    skills = json.dumps(skills, default=serialize_date)
    resume_query.update({"skills": skills})
    
    db.commit()
    skills = json.loads(db_resume.skills)
    return {"status": "success", "message": "Skill added successfully", "skills": skills}
    

@router.delete('/')
def delete_skill(resumeId: str, payload: schema.SkillBaseSchema, db: Session = Depends(get_db)):
    resume_query = db.query(Resume).filter(Resume.id == resumeId)
    db_resume = resume_query.first()

    if not db_resume:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'No resume with this id: {resumeId} found')
    skills = []
    if db_resume.skills is not None:
        skills = json.loads(db_resume.skills)
    payloadSkill = payload.dict(exclude_unset=True)

    # updates Date format payloadSkill
    # updates Date format payloadSkill
    if payloadSkill['last_used'] is not None:
        payloadSkill['last_used'] = payloadSkill['last_used'].isoformat() 
    
    # if not exists return
    if payloadSkill not in skills:
        return { "status": "success", "message": "Not Found Skill", "skills": skills, "payload" : payloadSkill }
        
    skills.remove(payloadSkill)
    skills = json.dumps(skills, default=serialize_date)
    resume_query.update({"skills": skills})
    
    db.commit()
    skills = json.loads(db_resume.skills)
    return { "status": "success", "message": "Skill deleted successfully", "status_code":status.HTTP_204_NO_CONTENT, "skills": skills }
