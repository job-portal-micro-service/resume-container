from app.resume.model import Resume
from app.education.schema import EducationBaseSchema
from sqlalchemy.orm import Session
from sqlalchemy import or_
from fastapi import Depends, HTTPException, status, APIRouter
from app.database import get_db
import json
from app.helpers import serialize_date

router = APIRouter()


@router.get('/')
def get_educations(resumeId: str, db: Session = Depends(get_db)):
    db_resume = db.query(Resume).filter(Resume.id == resumeId).first()
    if not db_resume:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"No resume with this id: {id} found")
    educations = []
    if db_resume.educations is not None:
        educations = json.loads(db_resume.educations)

    return {'status': 'success', 'results': len(educations), 'educations': educations}


@router.post('/', status_code=status.HTTP_201_CREATED)
def create_education(resumeId: str, payload: EducationBaseSchema, db: Session = Depends(get_db)):
    resume_query = db.query(Resume).filter(Resume.id == resumeId)
    db_resume = resume_query.first()

    if not db_resume:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'No resume with this id: {resumeId} found')
    educations = []
    if db_resume.educations is not None:
        educations = json.loads(db_resume.educations)
    payloadEducation = payload.dict(exclude_unset=True)

    # updates Date format payloadEducation
    if payloadEducation['duration_from'] is not None:
        payloadEducation['duration_from'] = payloadEducation['duration_from'].isoformat() 
    if payloadEducation['duration_to'] is not None:
        payloadEducation['duration_to'] = payloadEducation['duration_to'].isoformat() 

    # if exists return
    if payloadEducation in educations:
        return { "status": "success", "message": "Payload already exists", "educations": educations, "payload" : payloadEducation }
    
    # if input-education is not exists in list-educations
    educations.append(payloadEducation)
    educations = json.dumps(educations, default=serialize_date)
    resume_query.update({"educations": educations})
    
    db.commit()
    educations = json.loads(db_resume.educations)
    return {"status": "success", "message": "Education added successfully", "educations": educations}
    

@router.delete('/')
def delete_education(resumeId: str, payload: EducationBaseSchema, db: Session = Depends(get_db)):
    resume_query = db.query(Resume).filter(Resume.id == resumeId)
    db_resume = resume_query.first()

    if not db_resume:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'No resume with this id: {resumeId} found')
    educations = []
    if db_resume.educations is not None:
        educations = json.loads(db_resume.educations)
    payloadEducation = payload.dict(exclude_unset=True)

    # updates Date format payloadEducation
    # updates Date format payloadEducation
    if payloadEducation['duration_from'] is not None:
        payloadEducation['duration_from'] = payloadEducation['duration_from'].isoformat() 
    if payloadEducation['duration_to'] is not None:
        payloadEducation['duration_to'] = payloadEducation['duration_to'].isoformat() 
    
    # if not exists return
    if payloadEducation not in educations:
        return { "status": "success", "message": "Not Found Education", "educations": educations, "payload" : payloadEducation }
        
    educations.remove(payloadEducation)
    educations = json.dumps(educations, default=serialize_date)
    resume_query.update({"educations": educations})
    
    db.commit()
    educations = json.loads(db_resume.educations)

    return { "status": "success", "message": "Education deleted successfully", "status_code":status.HTTP_204_NO_CONTENT, "educations": educations }
