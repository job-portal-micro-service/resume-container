from datetime import date #, datetime 
from typing import Optional, Literal #, List
from pydantic import BaseModel #, validator, EmailStr
from app.constants import EducationType, EducationLabel

class EducationBaseSchema(BaseModel):
    degree: str 
    institute: str 
    duration_from: Optional[date] = None 
    duration_to: Optional[date] = None 
    label: Optional[Literal[EducationLabel]] = None
    type: Optional[Literal[EducationType]] = None


    # @validator('label')
    # def validate_label(cls, v):
    #     if v not in educationLabels:
    #         raise ValueError(f'Invalid choice. Must be one of: {educationLabels}')
    #     return v


    # @validator('type')
    # def validate_type(cls, v):
    #     if v not in educationTypes:
    #         raise ValueError(f'Invalid choice. Must be one of: {educationTypes}')
    #     return v