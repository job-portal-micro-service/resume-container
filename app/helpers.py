
from datetime import date

def serialize_date(obj):
    """Custom JSON serializer for date objects."""
    if isinstance(obj, date):
        return obj.isoformat()
    raise TypeError("Type not serializable")
