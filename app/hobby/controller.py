from app.resume.model import Resume
from app.hobby.schema import HobbyBaseSchema
from sqlalchemy.orm import Session
from sqlalchemy import or_
from fastapi import Depends, HTTPException, status, APIRouter
from app.database import get_db

router = APIRouter()


@router.get('/')
def get_hobbies(resumeId: str, db: Session = Depends(get_db)):
    db_resume = db.query(Resume).filter(Resume.id == resumeId).first()
    if not db_resume:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"No resume with this id: {id} found")
    hobbies = []
    if db_resume.hobbies is not None:
        hobbies = db_resume.hobbies.split(",")
    return {'status': 'success', 'results': len(hobbies), 'hobbies': hobbies}


@router.post('/', status_code=status.HTTP_201_CREATED)
def create_hobby(resumeId: str, payload: HobbyBaseSchema, db: Session = Depends(get_db)):
    resume_query = db.query(Resume).filter(Resume.id == resumeId)
    db_resume = resume_query.first()

    if not db_resume:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'No resume with this id: {resumeId} found')
    hobbies = []
    if db_resume.hobbies is not None:
        hobbies = db_resume.hobbies.split(",")
    payloadHobby = payload.dict(exclude_unset=True)

    # if input-hobby is not exists in list-hobbies
    if payloadHobby['title'] not in hobbies:
        hobbies.append(payloadHobby['title'])
    hobbies = ','.join(hobbies)
    resume_query.update({"hobbies": hobbies})
    db.commit()
    return {"status": "success", "message": "Hobby added successfully", "hobbies": hobbies}
    

@router.delete('/')
def delete_hobby(resumeId: str, payload: HobbyBaseSchema, db: Session = Depends(get_db)):
    resume_query = db.query(Resume).filter(Resume.id == resumeId)
    db_resume = resume_query.first()

    if not db_resume:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'No resume with this id: {resumeId} found')
    hobbies = []
    if db_resume.hobbies is not None:
        hobbies = db_resume.hobbies.split(",")
    payloadHobby = payload.dict(exclude_unset=True)

    # if input-hobby is exists in list-hobbies
    if payloadHobby['title'] in hobbies:
        hobbies.remove(payloadHobby['title'])
    hobbies = ','.join(hobbies)
    resume_query.update({"hobbies": hobbies})
    db.commit()
    return {"status": "success", "message": "Hobby deleted successfully", "status_code":status.HTTP_204_NO_CONTENT, "hobbies": hobbies}
