# from datetime import datetime, date
# from typing import List
from pydantic import BaseModel, HttpUrl, validator, EmailStr
from typing import Optional #, Dict


class ContactBaseSchema(BaseModel):
    address: str
    alternative_address: Optional[str] = None
    alternative_mobile: Optional[str] = None        # string: mobile
    alternative_email: Optional[EmailStr] = None    # email
    whatsapp: Optional[str] = None                  # string: mobile
    skype: Optional[HttpUrl] = None                 # string: skype 
    linkedin: Optional[HttpUrl] = None              # string: url 
    facebook: Optional[HttpUrl] = None              # string: url 
    github: Optional[HttpUrl] = None                # string: url 
    gitlab: Optional[HttpUrl] = None                # string: url 
    stackoverflow: Optional[HttpUrl] = None         # string: url 
    medium: Optional[HttpUrl] = None                # string: url 
    quora: Optional[HttpUrl] = None                 # string: url 
    website: Optional[HttpUrl] = None               # string: url 
    alternative_website: Optional[HttpUrl] = None   # string: url 
    
    
    @validator("linkedin")
    def validate_linkedin(cls, v):
        if v and not str(v).startswith("https://www.linkedin.com/"):
            raise ValueError("Invalid LinkedIn profile URL")
        return v
    
    @validator("skype")
    def validate_skype(cls, v):
        if v and not str(v).startswith("https://www.skype.com/"):
            raise ValueError("Invalid Skype profile URL")
        return v
    
    @validator("facebook")
    def validate_facebook(cls, v):
        if v and not str(v).startswith("https://www.facebook.com/"):
            raise ValueError("Invalid Facebook profile URL")
        return v
    
    @validator("github")
    def validate_github(cls, v):
        if v and not str(v).startswith("https://github.com/"):
            raise ValueError("Invalid GitHub profile URL")
        return v
    
    @validator("gitlab")
    def validate_gitlab(cls, v):
        if v and not str(v).startswith("https://gitlab.com/"):
            raise ValueError("Invalid GitLab profile URL")
        return v
    
    @validator("stackoverflow")
    def validate_stackoverflow(cls, v):
        if v and not str(v).startswith('https://stackoverflow.com/'):
            raise ValueError("Invalid Stack-Overflow profile URL")
        return v
    
    @validator("medium")
    def validate_medium(cls, v):
        if v and not str(v).startswith("https://medium.com/"):
            raise ValueError("Invalid Medium profile URL")
        return v
    
    @validator("quora")
    def validate_quora(cls, v):
        if v and not str(v).startswith("https://www.quora.com/"):
            raise ValueError("Invalid Quora profile URL")
        return v
    
    @validator("website")
    def validate_website(cls, v):
        # Your validation logic goes here
        # For example, you can check if the website starts with "http://" or "https://"
        if v and not str(v).startswith(("http://", "https://")):
            raise ValueError("Invalid URL format. URL must start with 'http://' or 'https://'")
        return v
    
    @validator("alternative_website")
    def validate_alternative_website(cls, v):
        # Your validation logic goes here
        # For example, you can check if the website starts with "http://" or "https://"
        if v and not str(v).startswith(("http://", "https://")):
            raise ValueError("Invalid URL format. URL must start with 'http://' or 'https://'")
        return v