from app.resume.model import Resume
from app.contact.schema import ContactBaseSchema
from sqlalchemy.orm import Session
from sqlalchemy import or_
from fastapi import Depends, HTTPException, status, APIRouter
from app.database import get_db
import json

router = APIRouter()


@router.get('/')
def get_contacts(resumeId: str, db: Session = Depends(get_db)):
    db_resume = db.query(Resume).filter(Resume.id == resumeId).first()
    if not db_resume:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"No resume with this id: {id} found")
    contacts = {}
    if db_resume.contacts is not None:
        contacts = json.loads(db_resume.contacts)
        # contacts = db_resume.contacts

    return {'status': 'success', 'results': len(contacts), 'contacts': contacts}


@router.post('/', status_code=status.HTTP_201_CREATED)
def update_contact(resumeId: str, payload: ContactBaseSchema, db: Session = Depends(get_db)):
    resume_query = db.query(Resume).filter(Resume.id == resumeId)
    db_resume = resume_query.first()

    if not db_resume:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'No resume with this id: {resumeId} found')

    # Serialize the payload to a dictionary    
    contacts = payloadContact = payload.dict(exclude_unset=True)
    payloadContact['skype'] = str(payloadContact['skype']) if payloadContact['skype'] is not None else None
    payloadContact['linkedin'] = str(payloadContact['linkedin']) if payloadContact['linkedin'] is not None else None
    payloadContact['facebook'] = str(payloadContact['facebook']) if payloadContact['facebook'] is not None else None
    payloadContact['github'] = str(payloadContact['github']) if payloadContact['github'] is not None else None
    payloadContact['gitlab'] = str(payloadContact['gitlab']) if payloadContact['gitlab'] is not None else None
    payloadContact['stackoverflow'] = str(payloadContact['stackoverflow']) if payloadContact['stackoverflow'] is not None else None
    payloadContact['medium'] = str(payloadContact['medium']) if payloadContact['medium'] is not None else None
    payloadContact['quora'] = str(payloadContact['quora']) if payloadContact['quora'] is not None else None
    payloadContact['website'] = str(payloadContact['website']) if payloadContact['website'] is not None else None
    payloadContact['alternative_website'] = str(payloadContact['alternative_website']) if payloadContact['alternative_website'] is not None else None
    
    # Update the contacts field of the resume with the provided payload
    payloadContact = json.dumps(payloadContact)
    resume_query.update({"contacts": payloadContact})
    
    # Commit changes to the database
    db.commit()
    return {"status": "success", "message": "Contact updated successfully", "contacts": contacts}
    
