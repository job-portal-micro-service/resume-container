from datetime import date #, datetime 
from typing import Optional, List
from pydantic import BaseModel #, validator, EmailStr
# from app.skill.schema import SkillBaseSchema as Skill

class ExperienceBaseSchema(BaseModel):
    job_title: str 
    organization: str 
    designation: Optional[str]
    department: Optional[str] 
    duration_from: Optional[date] = None 
    duration_to: Optional[date] = None 
    skills: Optional[List[str]] = None
    # skills: Optional[List[Skill]] = None

