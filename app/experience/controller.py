from app.resume.model import Resume
from app.experience.schema import ExperienceBaseSchema
from sqlalchemy.orm import Session
from sqlalchemy import or_
from fastapi import Depends, HTTPException, status, APIRouter
from app.database import get_db
import json
# from datetime import date
from app.helpers import serialize_date

router = APIRouter()


@router.get('/')
def get_experiences(resumeId: str, db: Session = Depends(get_db)):
    db_resume = db.query(Resume).filter(Resume.id == resumeId).first()
    if not db_resume:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"No resume with this id: {id} found")
    experiences = []
    if db_resume.experiences is not None:
        experiences = json.loads(db_resume.experiences)

    return {'status': 'success', 'results': len(experiences), 'experiences': experiences}


@router.post('/', status_code=status.HTTP_201_CREATED)
def create_experience(resumeId: str, payload: ExperienceBaseSchema, db: Session = Depends(get_db)):
    resume_query = db.query(Resume).filter(Resume.id == resumeId)
    db_resume = resume_query.first()

    if not db_resume:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'No resume with this id: {resumeId} found')
    experiences = []
    if db_resume.experiences is not None:
        experiences = json.loads(db_resume.experiences)
    payloadExperience = payload.dict(exclude_unset=True)

    # updates Date format payloadExperience
    if payloadExperience['duration_from'] is not None:
        payloadExperience['duration_from'] = payloadExperience['duration_from'].isoformat() 
    if payloadExperience['duration_to'] is not None:
        payloadExperience['duration_to'] = payloadExperience['duration_to'].isoformat() 

    # if exists return
    if payloadExperience in experiences:
        return { "status": "success", "message": "Payload already exists", "experiences": experiences, "payload" : payloadExperience }
    
    # if input-experience is not exists in list-experiences
    experiences.append(payloadExperience)
    experiences = json.dumps(experiences, default=serialize_date)
    resume_query.update({"experiences": experiences})
    
    db.commit()
    experiences = json.loads(db_resume.experiences)
    return {"status": "success", "message": "Experience added successfully", "experiences": experiences}
    

@router.delete('/')
def delete_experience(resumeId: str, payload: ExperienceBaseSchema, db: Session = Depends(get_db)):
    resume_query = db.query(Resume).filter(Resume.id == resumeId)
    db_resume = resume_query.first()

    if not db_resume:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'No resume with this id: {resumeId} found')
    experiences = []
    if db_resume.experiences is not None:
        experiences = json.loads(db_resume.experiences)
    payloadExperience = payload.dict(exclude_unset=True)

    # updates Date format payloadExperience
    # updates Date format payloadExperience
    if payloadExperience['duration_from'] is not None:
        payloadExperience['duration_from'] = payloadExperience['duration_from'].isoformat() 
    if payloadExperience['duration_to'] is not None:
        payloadExperience['duration_to'] = payloadExperience['duration_to'].isoformat() 
    
    # if not exists return
    if payloadExperience not in experiences:
        return { "status": "success", "message": "Not Found Experience", "experiences": experiences, "payload" : payloadExperience }
        
    experiences.remove(payloadExperience)
    experiences = json.dumps(experiences, default=serialize_date)
    resume_query.update({"experiences": experiences})
    
    db.commit()
    experiences = json.loads(db_resume.experiences)

    return { "status": "success", "message": "Experience deleted successfully", "status_code":status.HTTP_204_NO_CONTENT, "experiences": experiences }

# def serialize_date(obj):
#     """Custom JSON serializer for date objects."""
#     if isinstance(obj, date):
#         return obj.isoformat()
#     raise TypeError("Type not serializable")
