
# docker refresh & run
    docker-compose up -d --build
    docker-compose up -d
    docker-compose down


# with VENV
python3.11 -m venv venv && source venv/bin/activate

# required to install
pip install fastapi uvicorn sniffio SQLAlchemy pydantic python-dotenv mysql-connector-python pydantic[email]

# freeze installed items list into
pip freeze > requirements.txt

# install from requirements file (of txt type)
pip install -r requirements.txt

# run fastapi directly from venv
uvicorn app.main:app --host localhost --port 5555 --reload


# NOTE:
  pip install psycopg2
  prerequsits of psycopg2: sudo apt install libpq-dev python3-dev python3.11-dev

